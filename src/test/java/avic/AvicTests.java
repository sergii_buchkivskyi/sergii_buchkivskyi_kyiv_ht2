package avic;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class AvicTests {
    private WebDriver driver;

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "/Users/buchkivskyi/IdeaProjects/ta_task2_epam/src/main/resources/chromedriver");
    }

    @BeforeMethod
    public void testSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua");

    }

    @Test(priority = 1)
    public void checkSendingLetterToSupport() {
        driver.findElement(By.xpath("//*[@class='header-top__item lgreen-color js_addMessage_btn']")).click();
        driver.findElement(By.xpath("//a[text()='Служба поддержки']")).click();
        driver.findElement(By.xpath("//*[contains(@data-recipient-name, 'Служба поддержки')]//input[@name='user_name']")).sendKeys("testName");
        driver.findElement(By.xpath("//*[contains(@data-recipient-name, 'Служба поддержки')]//input[@name='user_email']")).sendKeys("test@gmail.com");
        driver.findElement(By.xpath("//*[contains(@data-recipient-name, 'Служба поддержки')]//textarea[@name='content']")).sendKeys("testText");
        driver.findElement(By.xpath("//*[contains(@data-recipient-name, 'Служба поддержки')]//*[@class='button-reset main-btn main-btn--green submit']")).click();
        driver.findElement(By.xpath("//div[@class='ttl color-green']")).isDisplayed();
    }

    @Test(priority = 2)
    public void checkMacbookM1AmountOnMacBook2020Page() {
        driver.findElement(By.xpath("//span[@class='sidebar-item']")).click();
        driver.findElement(By.xpath("//ul[contains(@class,'sidebar-list')]//a[contains(@href, 'apple-store')]")).click();
        driver.findElement(By.xpath("//div[@class='brand-box__info']//img[@alt='MacBook']")).click();
        driver.findElement(By.xpath("//label[@for='fltr-diagonal-ekrana-11-6']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//div[@class='filter-area js_filter_parent open-filter-tooltip']//span[@class='filter-tooltip-inner']")).click();
        List<WebElement> elementsList = driver.findElements(By.xpath("//div[@class='prod-cart height']"));
        int actualElementsSize = elementsList.size();
        assertEquals(actualElementsSize, 2);
    }

    @Test(priority = 3)
    public void checkSignUpWithInvalidPhoneNumber() {
        driver.findElement(By.xpath("//div[@class='header-bottom__right-icon']//i[@class='icon icon-user-big']")).click();
        driver.findElement(By.xpath("//a[@class='btn-offers']")).click();
        driver.findElement(By.xpath("//input[@class='validate' and @name='phone']")).sendKeys("1111111111");
        driver.findElement(By.xpath("//input[@data-validate='pass']")).sendKeys("123456");
        driver.findElement(By.xpath("//input[@data-validate='pass1']")).sendKeys("123456");
        driver.findElement(By.xpath("//button[@class='button-reset main-btn js_validate submit main-btn--green']")).click();
        driver.findElement(By.xpath("//div[@class='form-field input-field flex pass error']")).isDisplayed();
    }

    @Test(priority = 4)
    public void checkPriceSortingInDescendingOrder() {
        driver.findElement(By.xpath("//span[@class='sidebar-item']")).click();
        driver.findElement(By.xpath("//ul[contains(@class,'sidebar-list')]//a[contains(@href, 'apple-store')]")).click();
        driver.findElement(By.xpath("//div[@class='brand-box__info']//img[@alt='MacBook']")).click();
        driver.findElement(By.xpath("//div[@class='category-top']//span[@role='combobox']")).click();
        driver.findElement(By.xpath("//li[contains(@id, 'pricedesc')]")).click();
        List<WebElement> elementsList = driver.findElements(By.xpath("//div[@class='prod-cart__prise-new']"));
        boolean arrayIsSorted = true;
        String[] words;
        int[] arrayElements = new int [elementsList.size()];

        for (int i = 0; i < elementsList.size(); i++) {
            words = (elementsList.get(i).getText().split("[^0-9]"));
            arrayElements[i] = Integer.parseInt(words[0]);
        }

        for (int j = 1; j < arrayElements.length; j++) {
            if (arrayElements[j-1] < arrayElements[j]){
                arrayIsSorted = false;
                break;
            }
        }
        assertTrue(arrayIsSorted);
    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }
}

